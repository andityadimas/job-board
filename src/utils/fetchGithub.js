import fetch from 'node-fetch';
import formatQueryString from './formatQueryString';
import sortJobs from './sortJobs';
import removeDuplicatesBy from './removeDuplicatesBy';

export default async function fetchGithub({ searchTerm, location, type }) {
  console.log('\nFetching Github...');

  try {
    const query = formatQueryString(searchTerm);
    const loc = formatQueryString(location);
    const API_URL = `https://private-anon-ba712d6b3b-frontendtestmaukerja.apiary-mock.com/jobs?limit=${query}&location=${loc}&type=${type}`;

    const res = await fetch(API_URL);
    const jobs = await res.json();

    // sort by date
    const sortedJobs = sortJobs(jobs);
    const sortedUniqueJobs = removeDuplicatesBy(job => job.id, sortedJobs);

    console.log(`
      SearchTerm: ${searchTerm}
      Location: ${location}
      Type: ${type}
      Got a total of ${jobs.length} jobs (UNFILTERED)\n`);

    return sortedUniqueJobs;
  } catch (error) {
    console.warn(error);
    return new Error(error);
  }
}
